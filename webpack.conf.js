
var webpack = require('webpack');

module.exports = {
  entry   : {app: ['./src']},
  devtool : 'eval',
  output  : {
    path       : __dirname + '/dist/',
    filename   : '[name].js',
    publicPath : '/'
  },
  resolve   : {
    modulesDirectories : ['src', 'node_modules']
  },
  module    : {
    loaders : [{
      test    : /\.js$/,
      loaders : ['babel'],
      exclude : /node_modules/
    }, {
      test    : /\.less/,
      loaders : ['style', 'css?modules&localIdentName=[local]-[hash:base64:5]', 'autoprefixer', 'less']
    }]
  },
  plugins: [
    new webpack.NoErrorsPlugin(),
    new (require('html-webpack-plugin'))({
      filename : 'index.html',
      template : 'src/index.html'
    }),
    new (require('webpack-notifier'))()
  ],
  devServer: {proxy: {
    '/api': {
      target       : 'http://pokeapi.co',
      secure       : false,
      changeOrigin : true
    }
  }}
}
