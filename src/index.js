import React from 'react'
import {render} from 'react-dom'

import {applyMiddleware, createStore} from 'redux'
import {Provider} from 'react-redux'
import thunkMiddleware from 'redux-thunk'

import reducer from './reducers'
import App from './components/app'
import './index.less'

const store = createStore(reducer, {}, applyMiddleware(thunkMiddleware))

render((
  <Provider store={store}>
    <App />
  </Provider>
), document.getElementById('app'))
