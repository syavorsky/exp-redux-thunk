
const base = '/api/v2'

const delay = 0
const pageSize = 5

export const get = url => fetch(url, {
  headers : new Headers({'Content-type': 'application/json'}),
  cache   : 'cache',
  method  : 'GET'
}).then(resp => resp.ok
  ? resp.json()
  : Promise.reject(new Error(`${resp.status}: ${resp.statusText}`))
).then(json => (
  new Promise(resolve => setTimeout(resolve, delay, json))
)).catch(err => {
  console.warn('Invalid API reponse', err)
  return err
})

export const getList = page => get(`${base}/pokemon?offset=${page * pageSize}&limit=${pageSize}`)
  .then(resp => ({
    page  : page,
    items : resp.results,
    total : Math.ceil(resp.count / pageSize)
  }))
