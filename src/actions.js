import * as api from 'lib/api'

// data: [{
//   name : String,
//   url  : String
// }]
const _resetList = data => ({
  type: 'list:reset', data
})

// message: String
const _toggleLoader = message => ({
  type: 'loader:toggle', message
})

// data: {
//   name    : String,
//   sprites : Object
// }
const _resetDetails = data => ({
  type: 'details:reset', data
})

export const loadList = page => dispatch => {
  dispatch(_toggleLoader('Loading the list...'))
  return api.getList(page)
    .then(data => {dispatch(_resetList(data))})
    .then(() => {dispatch(_toggleLoader(null))})
}

export const loadDetails = name => (dispatch, getState) => {
  const item = getState().list.items
    .find(item => item.name === name)

  if (item) {
    dispatch(_toggleLoader(`Loading "${name}" details...`))
    return api.get(item.url)
      .then(data => {dispatch(_resetDetails(data))})
      .then(() => {dispatch(_toggleLoader(null))})
  }
}

export const loadPage = page => (dispatch, getState) => {
  return dispatch(loadList(page))
    .then(() => {
      const firstItem = getState().list.items[0]
      dispatch(_resetDetails(null))
      dispatch(loadDetails(firstItem.name))
    })
}
