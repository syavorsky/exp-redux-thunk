import {combineReducers} from 'redux'

const list = (state = {page: 0, total: 0, items: []}, action) => {
  switch (action.type) {
    case 'list:reset': return action.data
    default: return state
  }
}

const loader = (state = {message: null}, action) => {
  switch (action.type) {
    case 'loader:toggle': return {message: action.message}
    default: return state
  }
}

const details = (state = null, action) => {
  switch (action.type) {
    case 'details:reset': return action.data
    default: return state
  }
}

export default combineReducers({list, loader, details})
