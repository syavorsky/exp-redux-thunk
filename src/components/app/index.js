import React, {Component} from 'react'
import {connect} from 'react-redux'
import * as actions from 'actions'
import cls from './styles.less'

const undash = str => str.replace(/-/g, ' ')

const Loader = ({message}) => (
  <span className={cls.loader}>{message}</span>
)

const List = ({items, selected, onSelect}) => (
  <ul className={cls.list}>
    {items.map(item => (
      <li key={item.name}
        className={selected === item.name && cls.selected}
        onClick={() => onSelect(item.name)}>{item.name}</li>
    ))}
  </ul>
)

const Details = ({item, onPrev, onNext}) => (
  <div className={cls.details}>
    <h2 className={cls.centered}>{item.name}</h2>
    <div className={cls.centered}>
      <img src={item.sprites.front_default} />
      <img src={item.sprites.back_default} />
    </div>
  </div>
)

const Nav = ({page, total, loadPage}) => (
  <div className={cls.nav}>
    <button onClick={() => loadPage(page - 1)}
      disabled={page === 0}>prev</button>
    <span>{page}/{total}</span>
    <button onClick={() => loadPage(page + 1)}
      disabled={page === total}>next</button>
  </div>
)

class App extends Component {

  componentDidMount() {
    this.props.loadPage(0)
  }

  render() {
    const {list, loader, details, loadDetails, loadPage} = this.props
    return (
      <div className={cls.root}>
        <div className={cls.main}>
          <List items={list.items}
            selected={details && details.name}
            onSelect={loadDetails} />
          {details && (
            <Details item={details} />)}
        </div>
        <Nav page={list.page} total={list.total} loadPage={loadPage} />
        {loader.message && (
          <Loader message={loader.message} />)}
      </div>
    )
  }
}

export default connect(store => store, actions)(App)
